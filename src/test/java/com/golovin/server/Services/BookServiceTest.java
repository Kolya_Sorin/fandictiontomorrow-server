package com.golovin.server.Services;

import com.golovin.server.DAO.BookDao;
import com.golovin.server.classes.*;
import com.golovin.server.models.*;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {
    private static final int ID_BOOK = 7;

    @InjectMocks
    private static BookService bookService;

    @Mock
    private Session session;

    @Mock
    private BookDao bookDao;

    @Mock
    private UserService userService;

    @Mock
    private LibraryService libraryService;

    @Mock
    private ChaptersService chaptersService;

    @Mock
    private GenresService genresService;

    @Mock
    private LastBookService lastBookService;

    @Mock
    private CoverService coverService;

    @Mock
    private CompletenessService completenessService;

    @InjectMocks
    private MyCommon myCommon;

    private Chapters chapters1;

    private Chapters chapters2;

    private Chapters chapters3;

    private List<Books> bookList = new ArrayList<>();

    private List<Books> bookList2 = new ArrayList<>();

    private User user1;

    private User user2;

    private Books book1;

    private Books book2;

    private List<Library> libraryList = new ArrayList<>();

    private Status status1;

    private Status status2;

    private Library library1;

    private Library library2;

    List<Genres> genresList = new ArrayList<>();

    private Genres genresBooks;

    private Completeness completeness;

    private Cover cover;


    private int changeBook(int id_book, String name, String test) {
        return bookService.changeBook(id_book, new BookClass(name,
                2, test, 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), 1, false));
    }


    @BeforeEach
    void beforeEach() {
        Whitebox.setInternalState(BookService.class, "bookDao", bookDao);
        Whitebox.setInternalState(MyCommon.class, "chapterService", chaptersService);
        Whitebox.setInternalState(MyCommon.class, "userService", userService);
        Whitebox.setInternalState(MyCommon.class, "bookService", bookService);
        Whitebox.setInternalState(MyCommon.class, "lastBookService", lastBookService);
        Whitebox.setInternalState(MyCommon.class, "libraryService", libraryService);
        Whitebox.setInternalState(MyCommon.class, "genresService", genresService);
        Whitebox.setInternalState(MyCommon.class, "coverService", coverService);
        Whitebox.setInternalState(MyCommon.class, "completenessService", completenessService);


        user1 = new User("Name1", "Surname1", "auth1");
        user2 = new User("Name2", "Surname2", "auth2");
        genresBooks = new Genres("genre");
        genresBooks.setId(1);

        book1 = new Books(user1, "TestBook",
                "test???", genresBooks, new Completeness(),
                new Cover(), 18, Date.valueOf(LocalDate.now()), false);
        book1.setId(1);
        chapters1 = new Chapters("Name1", 1, "Text1", book1);
        chapters1.setId(1);
        book1.addChapter(chapters1);

        book2 = new Books();
        book2.setId(2);
        chapters2 = new Chapters("Name2", 1, "Text2", book2);
        chapters2.setId(2);
        chapters3 = new Chapters("Name3", 2, "Text3", book2);
        chapters3.setId(3);
        book2.addChapter(chapters2);
        book2.addChapter(chapters3);

        Books book3 = new Books(user1, "TestBook3",
                "test???3", genresBooks, new Completeness(),
                new Cover(), 20, Date.valueOf(LocalDate.now()), false);

        bookList.add(book1);
        bookList.add(book2);

        bookList2.add(book1);
        bookList2.add(book3);

        user1.setId(1);
        user1.addChapter(chapters1);
        user1.addChapter(chapters3);
        user2.setId(2);
        user2.addChapter(chapters3);

        status1 = new Status();
        status1.setId(1);
        status2 = new Status();
        status2.setId(2);

        library1 = new Library(new LibraryPK(book1.getId(), status1.getId(), user1.getId()), book1, status1, user1);
        library2 = new Library(new LibraryPK(book2.getId(), status1.getId(), user1.getId()), book2, status1, user1);

        libraryList.add(library1);
        libraryList.add(library2);


        for (int i = 1; i < 10; i++) {
            Genres genres = new Genres("genre" + i);
            genres.setId(i);
            genresList.add(genres);
        }


        completeness = new Completeness();
        completeness.setId(1);

        cover = new Cover("aaa");
        cover.setId(1);
    }

    @Test
    void getById() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findById(session, 1)).thenReturn(book1);
        Books book = bookService.getById(1);
        assertNotNull(book);
        assertEquals(book1.getId(), book.getId());
        assertEquals(book1.getName(), book.getName());
        assertEquals(book1.getAnnotation(), book.getAnnotation());
        assertEquals(book1.getViews(), book.getViews());
        assertEquals(book1.getAuthor(), book.getAuthor());
    }

    @Test
    void findAll() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findAll(session)).thenReturn(bookList);
        List<Books> booksListTest = bookService.findAll();
        booksListTest.sort(Comparator.comparingInt(Books::getId));
        assertNotNull(booksListTest);

        assertEquals(bookList.get(0).getName(), booksListTest.get(0).getName());
        assertEquals(bookList.get(0).getId(), booksListTest.get(0).getId());
        assertEquals(bookList.get(0).getDate(), booksListTest.get(0).getDate());
        assertEquals(bookList.get(0).getAuthor(), booksListTest.get(0).getAuthor());
        assertEquals(bookList.get(0).getCover(), booksListTest.get(0).getCover());
        assertEquals(bookList.get(0).getGenre(), booksListTest.get(0).getGenre());
        assertEquals(bookList.get(0).getAnnotation(), booksListTest.get(0).getAnnotation());
        assertEquals(bookList.get(0).getCompleteness(), booksListTest.get(0).getCompleteness());

        assertEquals(bookList.get(1).getName(), booksListTest.get(1).getName());
        assertEquals(bookList.get(1).getId(), booksListTest.get(1).getId());
        assertEquals(bookList.get(1).getDate(), booksListTest.get(1).getDate());
        assertEquals(bookList.get(1).getAuthor(), booksListTest.get(1).getAuthor());
        assertEquals(bookList.get(1).getCover(), booksListTest.get(1).getCover());
        assertEquals(bookList.get(1).getGenre(), booksListTest.get(1).getGenre());
        assertEquals(bookList.get(1).getAnnotation(), booksListTest.get(1).getAnnotation());
        assertEquals(bookList.get(1).getCompleteness(), booksListTest.get(1).getCompleteness());
    }


//    @ParameterizedTest
//    @ValueSource(ints = {1, 2, 4, 6, 8})
//    void isGenrePresentTrue(int genreId) {
//        when(bookDao.isGenrePresent(genreId)).thenReturn(true);
//        assertTrue(bookService.isGenrePresent(genreId));
//    }

//    @ParameterizedTest
//    @ValueSource(ints = {3, 5, 7})
//    void isGenrePresentFalse(int genreId) {
//        when(bookDao.isGenrePresent(genreId)).thenReturn(false);
//        assertFalse(bookService.isGenrePresent(genreId));
//    }

    @Test
    void getBooksFromLibraryWithStatus() {
        when(libraryService.getBooksFromLibraryWithStatus(library1.getStatus().getId(),
                library1.getUser().getId())).thenReturn(libraryList);
        ListBooksId listBooksId = bookService.getBooksFromLibraryWithStatus(library1.getStatus().getId(),
                library1.getUser().getId());
        assertEquals(2, listBooksId.getBooksId().size());
        assertEquals(2, listBooksId.getBooksId().get(1));
    }

//    @Test
//    void getPresentGenres() {
//        when(genresService.findAll()).thenReturn(genresList);
//        for (Genres genre : genresList) {
//            if (genre.getId() % 2 > 0) {
//                when(bookDao.isGenrePresent(genre.getId())).thenReturn(true);
//            } else {
//                when(bookDao.isGenrePresent(genre.getId())).thenReturn(false);
//            }
//        }
//        ListPresentBookInGenre listPresentBookInGenre = bookService.getPresentGenres();
//        assertEquals(9, listPresentBookInGenre.getGenres().size());
//        listPresentBookInGenre.getGenres().sort(Comparator.comparingInt(PresentBookInGenre::getGenre));
//        assertTrue(listPresentBookInGenre.getGenres().get(0).getIsPresent());
//        assertTrue(listPresentBookInGenre.getGenres().get(2).getIsPresent());
//        assertTrue(listPresentBookInGenre.getGenres().get(4).getIsPresent());
//        assertTrue(listPresentBookInGenre.getGenres().get(6).getIsPresent());
//        assertTrue(listPresentBookInGenre.getGenres().get(8).getIsPresent());
//        assertFalse(listPresentBookInGenre.getGenres().get(1).getIsPresent());
//        assertFalse(listPresentBookInGenre.getGenres().get(3).getIsPresent());
//        assertFalse(listPresentBookInGenre.getGenres().get(5).getIsPresent());
//        assertFalse(listPresentBookInGenre.getGenres().get(7).getIsPresent());
//    }

    @Test
    void getBook() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(bookDao.findById(session,1)).thenReturn(book1);
        doNothing().when(lastBookService).updateLastBooks(book1.getId(), user1.getId());
        doNothing().when(bookDao).update(session, book1);
        int views = book1.getViews();
        String date = book1.getDate().toString();

        when(bookDao.findById(session,1)).thenReturn(book1);

        when(bookDao.findById(session, 1)).thenReturn(book1);

        BookClass books = bookService.getBook(1, 1);
        assertEquals(book1.getAnnotation(), books.getAnnotation());
        assertEquals(book1.getAuthor().getId(), books.getAuthorId());
        assertEquals(book1.getGenre().getId(), books.getGenreId());
        assertEquals(book1.getCompleteness().getId(), books.getCompletenessId());
        assertEquals(views + 1, books.getViews());
        assertEquals(date, books.getDate());
        assertEquals(book1.getCover().getId(), books.getCoverId());
        assertFalse(books.getIsDraft());

        when(bookDao.findById(session, 10)).thenReturn(null);
        books = bookService.getBook(10, 1);
        assertEquals(" ", books.getAnnotation());
        assertEquals(0, books.getAuthorId());
        assertEquals(1, books.getGenreId());
        assertEquals(1, books.getCompletenessId());
        assertEquals(0, books.getViews());
        assertEquals("0", books.getDate());
        assertEquals(0, books.getCoverId());
        assertFalse(books.getIsDraft());

    }

    @Test
    void getMainPageBook() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findById(session, 1)).thenReturn(book1);
        MainPageBook books = bookService.getMainPageBook(1);
        assertEquals(book1.getName(), books.getName());
        assertEquals(book1.getAuthor().getId(), books.getAuthorId());
        assertEquals(book1.getCover().getId(), books.getCoverId());
        assertFalse(books.getIsDraft());

        when(bookDao.findById(session, 11)).thenReturn(null);
        books = bookService.getMainPageBook(11);
        assertNull(books);
    }

    @Test
    void getLibraryBook() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(libraryService.getUserBookStatus(1, 1))
                .thenReturn(library1.getStatus().getId());
        when(bookDao.getUserBook(session, 1, 1)).thenReturn(book1);

        LibraryBook books = bookService.getLibraryBook(1, 1);
        assertEquals(book1.getName(), books.getName());
        assertEquals(book1.getAuthor().getId(), books.getAuthorId());
        assertEquals(book1.getCover().getId(), books.getCoverId());
        assertEquals(books.getIsDraft(), books.getIsDraft());


        when(bookDao.getUserBook(session, 4, 4)).thenReturn(null);
        books = bookService.getLibraryBook(4, 4);
        assertEquals(5, books.getStatusId());
    }

    @Test
    void getSearchBookFirstSort() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findById(session, 1)).thenReturn(book1);
        SearchBook books = bookService.getSearchBook(1, 1);
        assertEquals(book1.getAnnotation(), books.getAnnotation());
        assertEquals(book1.getAuthor().getId(), books.getAuthorId());
        assertEquals(book1.getGenre().getId(), books.getGenre());
        assertEquals(book1.getViews(), books.getViews());
        assertEquals(Long.MAX_VALUE - book1.getViews(), books.getSort());
        assertEquals(book1.getCover().getId(), books.getCoverId());
        assertEquals(book1.isDraft(), books.getIsDraft());
    }

    @Test
    void getSearchBookSecondSort() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findById(session, 1)).thenReturn(book1);
        SearchBook books = bookService.getSearchBook(1, 2);
        assertEquals(book1.getAnnotation(), books.getAnnotation());
        assertEquals(book1.getAuthor().getId(), books.getAuthorId());
        assertEquals(book1.getGenre().getId(), books.getGenre());
        assertEquals(book1.getViews(), books.getViews());
        assertEquals(book1.getDate().getTime(), books.getSort());
        assertEquals(book1.getCover().getId(), books.getCoverId());
        assertEquals(book1.isDraft(), books.getIsDraft());
    }

    @Test
    void getSearchBookNull() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findById(session, 11)).thenReturn(null);
        SearchBook books = bookService.getSearchBook(11, 2);
        assertEquals(0, books.getAuthorId());
        assertTrue(books.getIsDraft());
    }


    @Test
    void getAuthorBooks() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.getAuthor(session, 1)).thenReturn((ArrayList<Books>) bookList2);
        ListBooksId books = bookService.getAuthorBooks(1);
        bookList2.sort(Comparator.comparingInt(Books::getId));
        books.getBooksId().sort(Integer::compareTo);
        assertEquals(bookList2.size(), books.getBooksId().size());
        assertEquals(bookList2.get(0).getId(), books.getBooksId().get(0));
    }

//    @Test
//    void getGenreBooks() {
//        when(bookDao.getGenre(1)).thenReturn((ArrayList<Books>) bookList2);
//        ListBooksId books = bookService.getGenreBooks(1);
//
//        bookList2.sort(Comparator.comparingInt(Books::getId));
//        books.getBooksId().sort(Integer::compareTo);
//        assertEquals(bookList2.size(), books.getBooksId().size());
//        assertEquals(bookList2.get(0).getId(), books.getBooksId().get(0));
//    }

    @Test
    void getChaptersOfBook() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findById(session, 2)).thenReturn(book2);
        ListChapterInBook chaptersOfBook = bookService.getChaptersOfBook(2);
        assertEquals(book2.getChaptersList().size(), chaptersOfBook.getChapters().size());
        assertEquals(book2.getChaptersList().get(0).getId(),
                chaptersOfBook.getChapters().get(0).getChapterId());
    }

    @Test
    void getBookInSection() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.getBookInSection(session, 1)).thenReturn(bookList);
        ListBooksId books = bookService.getBookInSection(1);
        when(bookDao.findAll(session)).thenReturn(bookList);
        List<Books> booksList = bookService.findAll();
        booksList.sort(Comparator.comparingInt(Books::getId));
        books.getBooksId().sort(Integer::compareTo);
        assertEquals(booksList.get(0).getId(), books.getBooksId().get(0));
        assertEquals(booksList.get(booksList.size() - 1).getId(), books.getBooksId().get(books.getBooksId().size() - 1));

        books = bookService.getBookInSection(3);
        assertTrue(books.getBooksId().isEmpty());
    }

//    @Test
//    void getBookInSectionTwo() {
//        ListBooksId books = bookService.getBookInSection(2);
//        List<Books> booksList = bookService.findAll();
//        when(bookDao.findAll()).thenReturn(bookList);
//        booksList.sort(Comparator.comparingInt(Books::getId));
//        books.getBooksId().sort(Integer::compareTo);
//        List<Books> hotNewBooks = new ArrayList<>();
//        for (Books book : booksList) {
//            if (book.getDate().toLocalDate().isAfter(LocalDate.now().minusMonths(1))) {
//                hotNewBooks.add(book);
//            }
//        }
//        hotNewBooks.sort(Comparator.comparingInt(Books::getId));
//        books.getBooksId().sort(Integer::compareTo);
//
//        assertEquals(booksList.get(0).getId(), books.getBooksId().get(0));
//        assertEquals(booksList.get(booksList.size() - 1).getId(), books.getBooksId().get(books.getBooksId().size() - 1));
//
//
//        books = bookService.getBookInSection(3);
//        assertTrue(books.getBooksId().isEmpty());
//    }

    @Test
    void addBook() {
        int coverId = 2;
        BookClass bookClass = new BookClass("Test Book",
                2, "test", 1, 1,
                1, Date.valueOf(LocalDate.now()).toString(), coverId, false);

        when(genresService.getById(1)).thenReturn(genresBooks);
        when(userService.getById(2)).thenReturn(user2);
        when(completenessService.getById(1)).thenReturn(completeness);

        int idBook = bookService.addBook(bookClass);
        assertEquals(0,idBook);

    }


    @Test
    void changeBook() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(genresService.getById(1)).thenReturn(genresBooks);
        when(completenessService.getById(1)).thenReturn(completeness);
        when(coverService.getById(1)).thenReturn(cover);
        when(bookDao.findById(session, 2)).thenReturn(book2);

        when(bookDao.openCurrentSessionWithTransaction()).thenReturn(session);
        doNothing().when(bookDao).update(any(Session.class), any(Books.class));
        int bookId = changeBook(2, "Test Book", "test");
        assertEquals(book2.getId(), bookId);
        assertEquals(book2.getGenre().getId(), genresBooks.getId());
        assertEquals(book2.getCompleteness().getId(), completeness.getId());
        //assertEquals(0, book.getViews());
        assertEquals(Date.valueOf(LocalDate.now()).toString(), book2.getDate().toString());
        assertEquals(1, book2.getCover().getId());
        assertFalse(book2.isDraft());


    }

    @RepeatedTest(5)
    void equivalenceInBounds() {
        int min = 1;
        int max = 150;
        String name = "a".repeat((int) (Math.random() * (max - min)) + min);
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(genresService.getById(1)).thenReturn(genresBooks);
        when(completenessService.getById(1)).thenReturn(completeness);
        when(coverService.getById(1)).thenReturn(cover);
        when(bookDao.findById(session, 2)).thenReturn(book2);

        int bookId = changeBook(book2.getId(), name, "test");
        assertEquals(name, book2.getName());
        assertEquals(book2.getId(), bookId);
    }

    @RepeatedTest(5)
    void equivalenceOutOfBoundsMore() {
        int min = 151;
        int max = Integer.MAX_VALUE;
        String name = "a".repeat((int) (Math.random() * (max - min)) + min);

        int bookId = changeBook(book2.getId(), name, "test");
        assertNotEquals(name, book2.getName());
        assertEquals(-1, bookId);
    }

    @Test
    void equivalenceOutOfBoundsLess() {
        String name = "";
        int bookId = changeBook(book2.getId(), name, "test");
        assertNotEquals(name, book2.getName());
        assertEquals(-1, bookId);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 151})
    void outOfBoundTest(int charCount) {
        String name = "a".repeat(charCount);
        int bookId = changeBook(book2.getId(), name, "test");
        assertNotEquals(name, book2.getName());
        assertEquals(-1, bookId);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 149, 150})
    void inBoundTest(int charCount) {
        when(bookDao.openCurrentSession()).thenReturn(session);
        String name = "a".repeat(charCount);
        when(genresService.getById(1)).thenReturn(genresBooks);
        when(completenessService.getById(1)).thenReturn(completeness);
        when(coverService.getById(1)).thenReturn(cover);
        when(bookDao.findById(session, 2)).thenReturn(book2);
        int bookId = changeBook(book2.getId(), name, "test");
        assertEquals(name, book2.getName());
        assertEquals(book2.getId(), bookId);
    }

    @Test
    void pairWiseValidValid() {
        String name = "a".repeat(75);
        String annotation = "b".repeat(300);
        when(genresService.getById(1)).thenReturn(genresBooks);
        when(completenessService.getById(1)).thenReturn(completeness);
        when(coverService.getById(1)).thenReturn(cover);
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.findById(session, 2)).thenReturn(book2);
        int bookId = changeBook(book2.getId(), name, annotation);

        assertEquals(name, book2.getName());
        assertEquals(annotation, book2.getAnnotation());
        assertEquals(book2.getId(), bookId);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "151,1001",
            "15,1001",
            "151,100"
    })
    void pairWiseInvalid(int nameCount, int annotationCount) {
        String name = "a".repeat(nameCount);
        String annotation = "b".repeat(annotationCount);
        int bookId = changeBook(book2.getId(), name, annotation);
        assertNotEquals(name, book2.getName());
        assertNotEquals(annotation, book2.getAnnotation());
        assertEquals(-1, bookId);
    }


    @Test
    void searchBooks() {
        SearchParamsClass searchParamsClass = new SearchParamsClass
                ("поиск", 2, 1);
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.searchBooks(session, searchParamsClass.getText(),searchParamsClass.getGenreId(),
                searchParamsClass.getSortingType())).thenReturn(bookList);
        ListBooksId books = bookService.searchBooks(searchParamsClass);
        assertEquals(bookList.get(0).getId(), books.getBooksId().get(0));
        assertEquals(2, books.getBooksId().size());
    }

    @Test
    void update() {
        ArgumentCaptor<Books> valueCapture = ArgumentCaptor.forClass(Books.class);
        when(bookDao.openCurrentSessionWithTransaction()).thenReturn(session);
        doNothing().when(bookDao).update(any(Session.class), valueCapture.capture());
        bookService.update(book2);
        assertEquals(book2.getName(), valueCapture.getValue().getName());
        assertEquals(book2.getViews(), valueCapture.getValue().getViews());
        assertEquals(book2.getId(), valueCapture.getValue().getId());

        bookService.update(book1);
        assertEquals(book1.getName(), valueCapture.getValue().getName());
        assertEquals(book1.getViews(), valueCapture.getValue().getViews());
        assertEquals(book1.getId(), valueCapture.getValue().getId());

    }

    @Test
    void setDraft() {
        when(bookDao.openCurrentSession()).thenReturn(session);
        when(bookDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(bookDao.findById(session, 2)).thenReturn(book2);
        doNothing().when(bookDao).update(session, book2);
        Boolean isDraft = bookService.setDraft(2);

        assertEquals(isDraft, book2.isDraft());
    }
}
