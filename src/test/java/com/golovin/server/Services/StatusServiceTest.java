package com.golovin.server.Services;

import com.golovin.server.DAO.StatusDao;
import com.golovin.server.models.Status;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class StatusServiceTest {
    @Mock
    private StatusDao statusDao;

    @Mock
    private Session session;

    @InjectMocks
    private StatusService statusService;

    @Test
    void getById() {
        Whitebox.setInternalState(StatusService.class,"statusDao",statusDao);


        Status status = new Status();
        status.setStatus("READING");
        status.setId(1);

        when(statusDao.openCurrentSession()).thenReturn(session);

        when(statusDao.findById(session, 1)).thenReturn(status);
        Status statusTest = statusService.getById(1);
        assertEquals(status.getStatus(),statusTest.getStatus());
        assertEquals(status.getId(),statusTest.getId());

        when(statusDao.findById(session, 11)).thenReturn(null);
        statusTest = statusService.getById(11);
        assertNull(statusTest);
    }

}