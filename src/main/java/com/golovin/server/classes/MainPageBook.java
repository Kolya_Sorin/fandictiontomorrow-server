package com.golovin.server.classes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MainPageBook {

    @JsonProperty
    private String name;

    @JsonProperty
    private int authorId;

    @JsonProperty
    private int coverId;

    @JsonProperty
    private boolean isDraft;

    public MainPageBook() {
    }

    public MainPageBook(String name, int authorId, int coverId, boolean isDraft) {
        this.name = name;
        this.authorId = authorId;
        this.coverId = coverId;
        this.isDraft = isDraft;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getCoverId() {
        return coverId;
    }

    public void setCoverId(int coverId) {
        this.coverId = coverId;
    }

    public boolean getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(boolean draft) {
        isDraft = draft;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
