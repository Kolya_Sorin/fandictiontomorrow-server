package com.golovin.server.models;

import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "жанры")
public class Genres {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "название_жанра")
    private String genre;

    @OneToMany(mappedBy = "genre")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Books> booksList;

    public Genres() {
    }

    public Genres(String genre) {
        this.genre = genre;
        booksList = new ArrayList<>();
    }

}
