package com.golovin.server.models;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
public class LastBookPK implements Serializable {

    @Column(name = "книга")
    private int bookId;

    @Column(name = "пользователь")
    private int userId;


    public LastBookPK(int bookId, int userId) {
        this.bookId = bookId;
        this.userId = userId;
    }

    public LastBookPK() {
    }
}
