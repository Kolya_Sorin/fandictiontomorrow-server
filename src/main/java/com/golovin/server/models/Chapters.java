package com.golovin.server.models;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@Table(name = "главы")
@EqualsAndHashCode
public class Chapters {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "название")
    private String name;

    @Column(name = "номер")
    private int number;

    @Column(name = "текст")
    private String text;

    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    @JoinColumn(name = "книга")
    private Books book;


    @ManyToMany(mappedBy = "userChaptersList")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> userList;

    public void addUser(User user){
        userList.add(user);
        user.getUserChaptersList().add(this);
    }


    public void removeUser(User user){
        userList.remove(user);
        user.getUserChaptersList().remove(this);
    }

    public Chapters() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chapters chapters = (Chapters) o;
        return number == chapters.number && Objects.equals(name, chapters.name) && Objects.equals(text, chapters.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number, text);
    }

    public Chapters(String name, int number, String text, Books book) {
        this.name = name;
        this.number = number;
        this.text = text;
        userList = new ArrayList<>();
        this.book = book;
    }

}
