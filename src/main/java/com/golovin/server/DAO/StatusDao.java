package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.StatusDaoInterface;
import com.golovin.server.models.Status;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class StatusDao implements StatusDaoInterface<Status, Integer> {


    private Session currentSession;

    private Transaction currentTransaction;

    public StatusDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public Status findById(Session session, Integer id) {
        Status status = (Status) getCurrentSession().get(Status.class, id);
        return status;
    }
}