package com.golovin.server.Services;

import com.golovin.server.DAO.CoverDao;
import com.golovin.server.models.Books;
import com.golovin.server.models.Cover;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class CoverService{

    private static CoverDao coverDao = new CoverDao();

    public CoverService() {
    }


    public Cover getById(int id) {
        Session session = coverDao.openCurrentSession();
        Cover cover = coverDao.findById(session, id);
        coverDao.closeCurrentSession(session);
        return cover;
    }

    private void persist(Cover cover) {
        Session session = coverDao.openCurrentSessionWithTransaction();
        coverDao.persist(session, cover);
        coverDao.closeCurrentSessionWithTransaction(session);
    }

    public void update(Cover cover) {
        Session session = coverDao.openCurrentSessionWithTransaction();
        coverDao.update(session, cover);
        coverDao.closeCurrentSessionWithTransaction(session);
    }

    public List<Cover> findAll() {
        Session session = coverDao.openCurrentSession();
        List<Cover> covers = coverDao.findAll(session);
        coverDao.closeCurrentSession(session);
        return covers;
    }


    public int addCover(String data){
        Cover cover = new Cover(data);
        persist(cover);
        return cover.getId();
    }

}